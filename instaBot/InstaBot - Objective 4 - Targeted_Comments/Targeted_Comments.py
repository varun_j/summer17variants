import requests

def post_a_comment(media_id, comment_text):
    payload = {"access_token": APP_ACCESS_TOKEN, "text" : comment_text}
    request_url = (BASE_URL + 'media/%s/comments') % (media_id)
    print 'POST request url : %s' % (request_url)

    make_comment = requests.post(request_url, payload).json()

    if make_comment['meta']['code'] == 200:
        print "Comment Successful."
    else:
        print "Error posting comment."

def post_targetted_comments(target_string, comment):
    request_url = BASE_URL + "tags/%s/media/recent?access_token=%s" %(target_string, APP_ACCESS_TOKEN)
    response = requests.get(request_url).json()

    print "Looking for tags: %s" %(request_url)

    if (response['meta']['code'] == 200):
        if len(response['data']) > 0:
            for cur_index in range(0, len(response['data'])):
                cur_id = response["data"][cur_index]["id"]
                post_a_comment(cur_id, comment)
        else:
            print "No posts found"
    else:
        print 'Error'