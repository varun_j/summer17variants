# Instagram API Basic

Code snippets for the additional objective:

1. Write code to get images between certain geographical coordinates, analyze the caption and determine if it is a natural calamity such as an earthquake, floods etc. 

2. Write code to compare negative and positive comments on a particular's posts and plot it on a pie chart.

3. Write code to determine the number of images shared with a particular hashtag and plot the same using matplotlib. 

4. Targeted commented on posts for the sake of marketing your product or service.

5. Find sub-trends for an event or activity and plot a word cloud for the same.

6. Determine a user's interests based on hashtag analysis of recent posts and plot the same using matplotlib.