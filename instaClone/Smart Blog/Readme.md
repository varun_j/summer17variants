# Smart Blog App (Instaclone)

Following is a variant of the Instaclone Application. This Smart Blog App is capable of categorizing the posted images automatically.
#### Steps to Use 
  - Signup to clarifai.com & generate your api key
  - Install Clarifai package for python by running this command 
  
    >   
        pip install clarifai
    
  - Now add the following line to top of views.py 
  
    >
        from clarifai.rest import ClarifaiApp
    
  - In views.py define a new variable and name it API_KEY
  - From your Clarifai Account copy the api key and paste it in views.py as the value of API_KEY variable between quotes.
  
    > 
        API_KEY="YOUR CLARIFAI API KEY"
   
 - In models.py add a new model, named as CategoryModel with following code 
 
    >
		class CategoryModel(models.Model):
			    post = models.ForeignKey(PostModel)
			    category_text = models.CharField(max_length=555)
	
 - In views.py add a new function, named as add_category with following code
 
    >
	    def add_category(post):
	    app = ClarifaiApp(api_key=API_KEY)
	    model = app.models.get("general-v1.3")
	    response = model.predict_by_url(url=post.image_url)
	    if response["status"]["code"] == 10000:
	        if response["outputs"]:
	            if response["outputs"][0]["data"]:
	                if response["outputs"][0]["data"]["concepts"]:
	                    for index in range(0, len(response["outputs"][0]["data"]["concepts"])):
	                        category = CategoryModel(post=post, category_text = response["outputs"][0]["data"]["concepts"][index]["name"])
	                        category.save()
	                else:
	                    print "No Concepts List Error"
	            else:
	                print "No Data List Error"
	        else:
	            print "No Outputs List Error"
	    else:
	        print "Response Code Error"
     
- In views.py call the add_category(post) function in post_view() after the post.save() line.

	>
		def post_view(request):
		 	 ----
			 ----
			 post.save()
			 add_category(post) 
		    
- In feeds.html copy the following code in <body> tag to display the categories of posted images.
	
	>
		<div>
		          <p> <b>Categories:</b> <br>
		                {% for category in post.categories %}
		                    {{ category.category_text }}
		                {% endfor %}
		          </p>
		</div>		 
	  
 - Now run the following two commands in terminal or cmd
	
	>
		python manage.py makemigrations
	    python manage.py migrate		 
	   
  - Congratulations!! Almost Done, now just run the last command
	
	>
		python manage.py runserver       
    
  - For any help just read the comments in the code