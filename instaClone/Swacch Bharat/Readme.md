
# Swacch Bharat (Instaclone)

Following is a variant of the Instaclone Application. This swacch bharat app is capable of verifying the trash/garbage/pits etc images posted by the people automatically.
#### Steps to Use 
  - Signup to clarifai.com & generate your api key
  - Install Clarifai package for python by running this command 
    ```sh
    pip install clarifai
    ```
  - Now add the following line to top of views.py 
    ```sh
    from clarifai.rest import ClarifaiApp
    ```
  - In views.py define a new variable and name it 
  - From your Clarifai Account copy the api key and paste it in views.py as the value of API_KEY variable between quotes.
    ```sh 
    API_KEY="YOUR CLARIFAI API KEY"
    ```
 - In models.py add a field in the post model
 
    ```sh
    is_verified= False

    ```
    if the tag output of the is inside the swacch list then is verified is true which means that the spot/post is real and need to be cleaned.

 - In views.py modify with the following code
     

    if response["status"]["code"] == 10000:
        if response["outputs"]:
            if response["outputs"][0]["data"]:
                if response["outputs"][0]["data"]["concepts"]:
                    for index in range(0, len(response["outputs"][0]["data"]["concepts"])):
                        if response["outputs"][0]["data"]["concepts"]:
                                concepts = response["outputs"][0]["data"]["concepts"]
                                for index in range(0, len(response["outputs"][0]["data"]["concepts"])):
                                    # for concept in concepts[:10]:
                                    tag = response["outputs"][0]["data"]["concepts"][index]['name']
                                    if tag in swacch_list:
                                        post.is_verified = True
                                        break

	and save the post as post.save()
	# list is added in the code 

 - Now run the following two commands in terminal or cmd
   ```sh
   python manage.py makemigrations
   python manage.py migrate
   ```
  - Congratulations!! Almost Done, now just run the last command
    ```sh
    python manage.py runserver
    ```
  - For any help just read the comments in the code
