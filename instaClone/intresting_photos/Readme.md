Objective 4 :: Intresting Photos

1. We are supposed to automatically tag the images uploaded by user.
2. The changes are Database are as follows :
	
	
		class HashTag(models.Model):
			name = models.CharField(max_length=120)

			
		class Hash2Post(models.Model):
			hashtag = models.ForeignKey(HashTag)
			post = models.ForeignKey(PostModel)
		
These are models are created for searching and storing of tags.

3. For the same purpose, we are using clarafai API with general model.
4. The tags are segregated from the JSON reply of the ASI call.
5. If these tags are not present in the HashTag Model, then the entry is created.
6. The URL 'search/' is updated, which shows only the post of q=<tag>
7. Final step is to render the data based of queries on DB 

Example tag search link :: **GET/search?q=child**