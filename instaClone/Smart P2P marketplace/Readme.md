#  SmartP2P MarketPlace(Instaclone)

Following is a variant of the Instaclone Application. In this SmartP2P MarketPlace App the user can upload the images of products of different brands and other users can review(comment) on the products, upvote and downvote the reviews and the user receive an email on every successful like and comment on his post 
#### Steps to Use 
  - Signup to clarifai.com & generate your api key
  - Install Clarifai package for python by running this command 

	>
		pip install clarifai

  - Now add the following line to top of views.py 
    
	>
    	from clarifai.rest import ClarifaiApp
    
  - In views.py define a new variable and name it 
  - From your Clarifai Account copy the api key and paste it in views.py as the value of API_KEY variable between quotes.
    
	>
    	API_KEY="YOUR CLARIFAI API KEY"
	
  - Signup to sendgrid.com 
  - Install Sendgrid package for python by running this command 

	>
		pip install sendgrid

  - In Settings.py copy the following code
    
	>
		EMAIL_HOST = 'smtp.sendgrid.net'
		EMAIL_HOST_USER = 'Your Sendgrid Email Id'
		EMAIL_HOST_PASSWORD = 'Your Sendgrid Password'
		EMAIL_PORT = 587
		EMAIL_USE_TLS = True   
    
 - In models.py in the add UpvoteModel and CategoryModel with the following code 
 
    >
    	class UpvoteModel(models.Model):
		    user            = models.ForeignKey(UserModel)
		    comment         = models.ForeignKey(CommentModel)
		    created_on      = models.DateTimeField(auto_now_add=True)
		    updated_on      = models.DateTimeField(auto_now=True)
	>
		class CategoryModel(models.Model):
		    post = models.ForeignKey(PostModel)
		    category_text = models.CharField(max_length=555)
   
 - In models.py add following code to PostModel
 	
	>
		@property
   		def categories(self):
        return CategoryModel.objects.filter(post=self)

 - In models.py add following code to CommentModel
 	
	>  
		has_upvoted=False
	>
		@property
    	def upvote_count(self):
        return len(UpvoteModel.objects.filter(comment=self))

 - In views.py add a new function, named as upvote with following code
 
	>
		def upvote(request):
		    user = check_validation(request)
		    if user and request.method=='POST':
		        form = UpvoteForm(request.POST)
		        if form.is_valid():
		            print 'form valid'
		            comment_id = form.cleaned_data.get('comment').id
		            print comment_id
		            existing_upvote = UpvoteModel.objects.filter(comment_id=comment_id, user=user).first()
		            print existing_upvote
		            if existing_upvote:
		                existing_upvote.delete()
		            else:
		                print 'Create Upvote'
		                post = UpvoteModel.objects.create(comment_id=comment_id, user=user)
		                print post
		                print UpvoteModel.objects.filter(comment=comment_id)
		                post.save()
		            return redirect('/feed/')
		        else:
		            print 'Form not valid'
		            return redirect('/feed/')
		    else:
		        return redirect('/login/')

 - In views.py add a new function, named as Category_View with following code
    
	>
		def Category_View(post):
		    app = ClarifaiApp(api_key=API_KEY)  # passing api key of clarifai
		    model = app.models.get("general-v1.3")  # setting clarifai to f=general classification results
		    response = model.predict_by_url(url=post.image_url)  # receiving response of clarifai
		    if response["status"]["code"] == 10000:
		        print response
		        if response["outputs"]:  # if the output in response are true
		            if response["outputs"][0]["data"]:
		                if response["outputs"][0]["data"]["concepts"]:
		                    for index in range(0, len(response["outputs"][0]["data"]["concepts"])):
		                        category = CategoryModel(post=post,category_text=response["outputs"][0]["data"]["concepts"][index]["name"])
		                        category.save()
		                else:
		                    print "No Concepts List Error"
		            else:
		                print "No Data List Error"
		        else:
		            print "No Outputs List Error"
		    else:
		        print "Response Code Error"
    
 - In views.py call the Category_View(post) function in post_view() after the post.save() line.
    
	>
		def post_view(request):
	         ----
	         ----
	         post.save()
	         Category_View(post)
   
 - In feeds.html copy the following code in <body> tag to display the categories of posted images.
	
	>
		 <div>
        	{% for category in post.categories %}
                {{ category.category_text }}
            {% endfor %}
        </div>

	>
		<div>
                <form method="post" action='/upvote/'>
                  {% csrf_token %}
                  <input type="hidden" name="comment" value="{{ comment.id }}">
                  {% if comment.has_upvoted %}
                    <button class="btn btn-default btn-upvote">Downvote <i class="fa fa-thumbs-o-up" aria-hidden="true"></i></button>
                  {% else %}
                    <button class="btn btn-default btn-upvote">Upvote <i class="fa fa-thumbs-o-up" aria-hidden="true"></i></button>
                  {% endif %}
                </form>
                <div>
                  {{ comment.upvote_count }} Upvotes
                </div>
        </div>

 - In views.py feed_view add the following code
 
	>
		 comments = CommentModel.objects.filter(post_id=post.id,user=user)
            for comment in comments:
                existing_upvote = UpvoteModel.objects.filter(comment=comment.id).first()
                print existing_upvote
                if existing_upvote:
                    comment.has_upvoted = True

 - In views.py add the following code to like_view to send email on every successful like
 
	>
		----
		----
		send_mail(
                    'Heyy, You got a like from ' + post.user.name,
                    'Check it out at smartp2pmarketplace.com',
                    'smartp2pmarketplace.com',
                    [post.post.user.email],
                    fail_silently=False,
                )
			----
			----
            return redirect('/feed/')
 - In views.py add the following code to comment_view to send email on every successful comment
 
	>
		----
		----
		send_mail(
                    'Heyy, You got a comment from ' + post.user.name,
                    'Check it out at smartp2pmarketplace.com',
                    'smartp2pmarketplace.com',
                    [post.post.user.email],
                    fail_silently=False,
                )
			----
			----
            return redirect('/feed/')

 - Now run the following two commands in terminal or cmd
	
	>
		python manage.py makemigrations
		python manage.py migrate		   
   
 - Congratulations!! Almost Done, now just run the last command
	
	>
		python manage.py runserver
   
 - For any help just read the comments in the code