from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from datetime import datetime
from forms import *
from models import *
from django.contrib.auth.hashers import make_password, check_password
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
import smtplib
from email.mime.text import MIMEText as text
from imgurpython import ImgurClient
from marketplace.settings import BASE_DIR
import os
import sendgrid
from sendgrid.helpers.mail import *
from clarifai.rest import ClarifaiApp
import json
from datetime import datetime

YOUR_CLIENT_ID = " "
YOUR_CLIENT_SECRET = " "

API_KEY=""


# View to the landing page
def landing(request):
    return render(request,'landing.html')


# View to the home page

def signup(request):
    today = datetime.now()
    if request.method == 'POST':
        signup_form = SignUpForm(request.POST)

        if signup_form.is_valid():
            # Extract the details from the form
            username = signup_form.cleaned_data['username']
            name = signup_form.cleaned_data['name']
            email = signup_form.cleaned_data['email']
            password = signup_form.cleaned_data['password']

            #Saving data to the database
            user = UserModel(name=name, password=make_password(password), email=email, username=username)
            user.save()
            #Show the success page
            return render(request, 'success.html')
        else:
            print 'Error occured while signing up'
            return render(request,'home.html',{'context': signup_form.errors})

    else:
        signup_form = SignUpForm()

    # Render the home page
    return render(request,'home.html',{'signup_form': signup_form})


# View for the login page

def login(request):

    dict = {}

    if request.method == 'POST':

        form = LoginForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            user = UserModel.objects.filter(username=username).first()


            if user:
                if check_password(password, user.password):
                    # User is Valid
                    print 'Valid'
                    token   =  SessionToken(user=user)
                    token.create_token()
                    token.save()



                    response = redirect('/feed/')

                    response.set_cookie(key='session_token', value=token.session_token)
                    return response


                else:
                    # User is not valid
                    print 'Invalid User'
                    return render(request,'login.html',{'context': 'Your password is not correct! Try Again!'})
            else:
                # User does not exist'
                print 'User doesnt exist'
                return render(request,'login.html',{'context': 'Username not registered'})
        else:
            # Form is not Valid
            print 'Invalid Form'
            return render(request,'login.html',{'context': 'Could not log you in. Please fill all the fields correctly'})
    else:
        form = LoginForm()

    return render(request,'login.html')


# Check if the current session is valid
def check_validation(request):
    if request.COOKIES.get('session_token'):
        session = SessionToken.objects.filter(session_token=request.COOKIES.get('session_token')).first()
        if session:
            return session.user
    else:
        return None




# def post_view(request):
def feed(request):
    user = check_validation(request)

    if user:
        if request.method == 'GET':
            form = PostForm()
            return render (request, 'feed.html', {'form': form})
        elif request.method == 'POST':
            form = PostForm(request.POST, request.FILES)

            if form.is_valid():
                image = form.cleaned_data.get('image')
                caption = form.cleaned_data.get('caption')
                post = PostModel(user=user, image=image, caption=caption)
                post.save()

                path = str(BASE_DIR + '\\' + post.image.url)

                client = ImgurClient(YOUR_CLIENT_ID, YOUR_CLIENT_SECRET)
                post.image_url = client.upload_from_path(path, anon=True)['link']
                post.save()

                Category_View(post)




                return render(request,'feed_new.html',{'post': post})
        else:
            return redirect('/login/')
    else:
        # If the user is not logged in
        return redirect('/login/')

# Main feed View
def feed_main(request):
    # Validates if the user is logged in or not
    user = check_validation(request)
    print '----Feed Main------'
    if user:
        posts = PostModel.objects.all().order_by('-created_on')
        # posts = PostModel.objects.all().order_by('-tags')

        for post in posts:
            existing_like = LikeModel.objects.filter(post_id=post.id, user=user).first()

            comments = CommentModel.objects.filter(post_id=post.id,user=user)
            for comment in comments:
                existing_upvote = UpvoteModel.objects.filter(comment=comment.id).first()
                print existing_upvote

                if existing_upvote:
                    comment.has_upvoted = True



            # If user has liked the post set the boolean value to True
            if existing_like:
                post.has_liked = True


        return render(request,'feed_main.html',{
            'posts': posts
        })
    else:
        return redirect('/login/')


# Like view
def like(request):
    user = check_validation(request)
    if user and request.method=='POST':
        form = LikeForm(request.POST)

        if form.is_valid():
            post_id = form.cleaned_data.get('post').id
            existing_like = LikeModel.objects.filter(post_id=post_id, user=user).first()

            # If user has already registered a like, then delete it
            if existing_like:
                existing_like.delete()
            else:
                # Otherwise create a like
                post = LikeModel.objects.create(post_id=post_id, user=user)
                print str(post.post.user.email)
                print str(post.user.name)
                send_mail(
                    'Heyy, You got a like from ' + post.user.name,
                    'Check it out at smartp2pmarketplace.com',
                    'smartp2pmarketplace.com',
                    [post.post.user.email],
                    fail_silently=False,
                )



            return redirect('/feed/')
    else:
        return redirect('/login/')





# Comment View
def comment(request):
    user = check_validation(request)

    if user and request.method=='POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            post_id = form.cleaned_data.get('post').id
            comment_text = form.cleaned_data.get('comment_text')

            # Create a CommentModel object and save it in the database
            comment = CommentModel.objects.create(user=user, post_id=post_id, comment_text=comment_text)
            comment.save()

            # Send email to user on a successful comment
            print comment.user.name
            print comment.post.user.email
            send_mail(
                'Heyy, You got a comment from ' + comment.user.name,
                'Check it out at smartp2pmarketplace.com',
                'smartp2pmarketplace.com',
                [comment.post.user.email],
                fail_silently=False,
            )

            return redirect('/feed')
        else:
            return redirect('/feed/')
    else:
        return redirect('/login')


# View to log the user out
def logout(request):
    user = check_validation(request)
    if user:
        response = redirect('/')
        response.delete_cookie(key='session_token')
        return response
    else:
        return redirect('/feed/')


# Upvote view
def upvote(request):
    user = check_validation(request)
    if user and request.method=='POST':
        form = UpvoteForm(request.POST)

        if form.is_valid():
            print 'form valid'
            comment_id = form.cleaned_data.get('comment').id
            print comment_id
            existing_upvote = UpvoteModel.objects.filter(comment_id=comment_id, user=user).first()
            print existing_upvote

            # If user has already registered an upvote, then delete it
            if existing_upvote:
                existing_upvote.delete()
            else:
                # Otherwise create an upvote
                print 'Create Upvote'
                post = UpvoteModel.objects.create(comment_id=comment_id, user=user)
                print post
                print UpvoteModel.objects.filter(comment=comment_id)
                post.save()

            return redirect('/feed/')
        else:
            print 'Form not valid'
            return redirect('/feed/')
    else:
        return redirect('/login/')

def Category_View(post):
    app = ClarifaiApp(api_key=API_KEY)  # passing api key of clarifai
    model = app.models.get("general-v1.3")  # setting clarifai to f=general classification results
    response = model.predict_by_url(url=post.image_url)  # receiving response of clarifai
    # on aa successful request clarifai gives 10000 as a status code
    if response["status"]["code"] == 10000:
        print response
        if response["outputs"]:  # if the output in response are true
            if response["outputs"][0]["data"]:
                if response["outputs"][0]["data"]["concepts"]:
                    for index in range(0, len(response["outputs"][0]["data"]["concepts"])):
                        # passing all categories in the category model and the saving them to database
                        category = CategoryModel(post=post,
                                                 category_text=response["outputs"][0]["data"]["concepts"][index][
                                                     "name"])
                        category.save()
                else:
                    print "No Concepts List Error"
            else:
                print "No Data List Error"
        else:
            print "No Outputs List Error"
    else:
        print "Response Code Error"