Social Kids : Make a social network that is safe for kids. It should make sure that images and text is appropriate for kids under the age of 12. - Clarifai for photos, paralleldots for comments


-> in views.py
INAPPROPRIATE_WORDS=['arse','arsehole','ass','asshole','badass','bastard','beaver','bitch','bollock','bollocks','boner','bugger','bullshit','bum','cock','crap','creampie','cunt','dick','dickhead','dyke','fag','faggot','fart','fatass',
'fuck','fucked','fucker','fucking','holy shit','jackass','jerk off','kick ass','kick-ass','kike','kikes','nigga','nigger','piss',
'pissed','pizza nigger','shit','shittier','shittiest','shitty','son of a bitch','sons of bitches','STFU','suck','tit','trap','twat','wan']


->adding this to views.py in the comment functionality to check for the curse words
comment_text = form.cleaned_data.get('comment_text')
            comment_text_words=TextBlob(comment_text).words
            for word in comment_text_words:
                if word in INAPPROPRIATE_WORDS:
                    error_message="You are trying to add an inapproprite comment!!"
                    return render(request,'error.html',{'error_message':error_message})



->this in create post function to disallow user to post nudity content

app = ClarifaiApp(api_key=CLARIFAI_API_KEY)
                model = app.models.get('nsfw-v1.0')
                response = model.predict_by_url(url=post.image_url)
                concepts_value=response['outputs'][0]['data']['concepts']
                for i in concepts_value:
                    if i['name']=='nsfw':
                        nudity_level=i['value']
                        if nudity_level>=0.85:
                            print response['outputs'][0]['data']['concepts']
                            print nudity_level
                            post.delete()
                            error_message="You are trying to post an inappropriate photo!!"
                            return render(request,"socialkidsapp/templates/error.html",{'error_message':error_message})
                        else:
                            return redirect('/feed/')

->creating error.html

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>WARNING</title>
    <style>
        h2{
            color: red;
        }
    </style>
</head>
<body>
<h2>{{ error_message }}</h2>

</body>
</html>

further read comments in the code

