#  Upload TO Win(Instaclone)

Following is a variant of the Instaclone Application. In this Upload To Win App the user can upload the images of proiducts of different brands and win points.
#### Steps to Use 
  - Signup to clarifai.com & generate your api key
  - Install Clarifai package for python by running this command 

	>
		pip install clarifai
    
  - Now add the following line to top of views.py 
    
	>
    	from clarifai.rest import ClarifaiApp
    
  - In views.py define a new variable and name it 
  - From your Clarifai Account copy the api key and paste it in views.py as the value of API_KEY variable between quotes.
    
	>
    	API_KEY="YOUR CLARIFAI API KEY"
    
 - In models.py in the UserModel add the following code 
 
    >
    	points = models.IntegerField(default=0)
   
 - In views.py add a new function, named as award_points with following code
    
	>
	    def award_points(post,user):
	    url = post.image_url  
	    app = ClarifaiApp(api_key=API_KEY)  
	    model = app.models.get('logo')  logos in the image
	    image = Image(url=url)
	    response = model.predict([image]) 
	    data = response['outputs'][0]['data']['regions'][0]['data']['concepts'][0]['name']
	    print data
	    if data == "Starbucks":   
	        userpoints = user.points + 20 
	        print userpoints
	        user.points = userpoints     
	        user.save()
	        print user
	    elif data == "Apple Inc":
	        userpoints = user.points + 50
	        print userpoints
	        user.points = userpoints
	        user.save()
	    elif data == "Dove":
	        userpoints = user.points + 70
	        print userpoints
	        user.points = userpoints
	        user.save()
	    elif data == "Burger King":
	        userpoints = user.points + 150
	        print userpoints
	        user.points = userpoints
	        print userpoints
	        user.save()
    
 - In views.py call the award_points(post,user) function in post_view() after the post.save() line.
    
	>
		def post_view(request):
	         ----
	         ----
	         post.save()
	         award_points(post,user)
   
 - In feeds.html copy the following code in <body> tag to display the categories of posted images.
	
	>
		<div>
             <p> 
                <b>Points For {{ post.user.username }}: </b> 
                {{ post.user.points }}
             </p>
        </div>  

 - Now run the following two commands in terminal or cmd
	
	>
		python manage.py makemigrations
		python manage.py migrate		   
   
 - Congratulations!! Almost Done, now just run the last command
	
	>
		python manage.py runserver
   
  - For any help just read the comments in the code