Make an application where users can upload images of products along with reviews in the caption.
Objective 6 :: Review this

We are supposed to automatically analyse the reviews uploaded by the user and marking them as positive or negative using parallel dots Api.
The changes are Database are as follows :

class PostModel(models.Model):
	#add 
	is_positive = models.BooleanField(default=False)



->inside post view
+set_api_key('your dots api key')
+review = sentiment(caption)


+if review > 0.5:
	is_positive = True
+else:
    is_positive = False

                
-post = PostModel(user=user, image=image, caption=caption)
+post = PostModel(user=user, image=image, caption=caption, is_positive=is_positive)


These two changes are done to store the review as positive or negative.

and if you want to show the corresponding action in feed.html, You can alter the code as:

 +        {% if post.is_positive %}
 +            <p>any specific action</p>
 +        {% else %}
 +            <p>other action!</p>
 +        {% endif %}


# for further details see the comments in the code
