"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from reviewthisapp.views import signup_view, login_view, feed_view, post_view, like_view, comment_view

urlpatterns = [
    # url structure to create post
    url(r'^post/', post_view),
    # url structure for the feed view
    url(r'^feed/', feed_view),
    # url structure when a post is liked
    url(r'^like/', like_view),
    # url structure when a post is commented
    url(r'^comment/', comment_view),
    # for login view
    url(r'^login/', login_view),
    # homepage as signyp page
    url('', signup_view)
]
